const http = require("http");

const port = 4000;

const server = http.createServer((req, res) =>{
  // http method of the incoming requests can be accessed via "req.method"
    // GET Method - retrieving/reading information
  if(req.url === "/items" && req.method === "GET"){
    res.writeHead(200, {"Content-Type": "text/plain"});
    res.end("Data retrieved from the Database")
  }
  
  if(req.url === "/items" && req.method === "POST"){
    res.writeHead(200, {"Content-Type": "text/plain"});
    res.end("Data to be sent to the database")
  }
  /* 
    create "items" url with POST method. The response should be "Data to be sent to the database" with 200 as status code and plain text as the content type
  */

    // PUT (Update)
    if(req.url === "/items" && req.method === "PUT"){
      res.writeHead(200, {"Content-Type": "text/plain"});
      res.end("Update resources")
    }

    // DELETE
    if(req.url === "/items" && req.method === "DELETE"){
      res.writeHead(200, {"Content-Type": "text/plain"});
      res.end("Delete resources")
    } 
});

server.listen(port);

console.log(`Server is running at localhost: ${port}`);